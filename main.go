package main

import (
	"fmt"
	"math/rand"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/kelseyhightower/envconfig"
	influxdb "github.com/influxdata/influxdb-client-go"
	"log"
	"context"
	"encoding/json"
	"time"
)

type Config struct {
	InfluxURL    string `default:"https://us-west-2-1.aws.cloud2.influxdata.com"`
	InfluxToken  string
	InfluxBucket string
	InfluxOrg    string
	MqttURL      string `default:"tcp://mqtt:1883"`
	MqttTopic    string `default:"power"`
	MqttUser     string `default:""`
	MqttPass     string `default:""`
}

func main() {
	log.Printf("starting MQTT-InfluxDB consumer")
	var (
		ctx = context.Background()
		conf = Config{}
	)
	if err := envconfig.Process("", &conf); err != nil {
		log.Fatal(err)
	}
	log.Printf("configuration: %+v\n", conf)

	// influx cloud writer
	influx := influxdb.NewClient(conf.InfluxURL, conf.InfluxToken)
	influxWriter := influx.WriteAPIBlocking(conf.InfluxOrg, conf.InfluxBucket)

	// mqtt consumer
	opts := mqtt.NewClientOptions().AddBroker(conf.MqttURL).SetClientID(
		fmt.Sprintf("mqtt-influx-consumer-%d", rand.Int()))

	if conf.MqttUser != "" && conf.MqttPass != "" {
		opts = opts.SetUsername(conf.MqttUser)
		opts = opts.SetPassword(conf.MqttPass)
	}
	cl := mqtt.NewClient(opts)
	if token := cl.Connect(); token.Wait() && token.Error() != nil {
		log.Fatal(token.Error()) // fixme: what happens if mqtt broker goes kaput?
	}

	mqttCallback := func(c mqtt.Client, m mqtt.Message) {
		var data map[string]interface{}
		if err := json.Unmarshal(m.Payload(), &data); err != nil {
			log.Printf("error unmarshaling JSON from mqtt: %+v", err)
			return
		}
		point := influxdb.NewPoint(
			conf.MqttTopic,
			map[string]string{
				"sensorName": "temperature", // fixme: hardcoded
			},
			data,
			time.Now(), // fixme: use messsage timestamp
		)
		influxWriter.WritePoint(ctx, point)
	}

	if token := cl.Subscribe(conf.MqttTopic, 0, mqttCallback); token.Wait() && token.Error() != nil {
		log.Fatal(token.Error()) // fixme: what happens if mqtt broker goes kaput?
	}

	<-ctx.Done()
}
