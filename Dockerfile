FROM golang:1.15.0 as builder

WORKDIR /workspace
COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -o mqtt-influx-consumer

FROM gcr.io/distroless/static:nonroot

WORKDIR /
COPY --from=builder /workspace/mqtt-influx-consumer .
USER nonroot:nonroot

