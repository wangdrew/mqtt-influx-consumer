## MQTT InfluxDB consumer
Simple MQTT consumer that assumes messages are a flat JSON object (no nested objects) and dumps that into InfluxDB cloud

### Running in docker compose:
```
version: "3"
services:
  mqtt-influx-consumer:
    image: registry.gitlab.com/wangdrew/mqtt-influx-consumer
    container_name: mqtt-influx-consumer
    env_file: conf.env
    restart: unless-stopped
    entrypoint: ["/mqtt-influx-consumer"]
```

