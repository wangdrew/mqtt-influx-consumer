module gitlab.com/wangdrew/mqtt-influx-consumer

go 1.15

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0 // indirect
	github.com/influxdata/influxdb-client-go v1.4.0 // indirect
	github.com/kelseyhightower/envconfig v1.4.0 // indirect
	golang.org/x/net v0.0.0-20200813134508-3edf25e44fcc // indirect
)
